import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';

import { AppComponent }  from './app.component';
import { ItemDetailComponent } from './item-detail.component';
import { NewsComponent } from './news.component';
import { ItemService } from './item.service';
import { DashboardComponent } from './dashboard.component';
import { ItemSearchComponent } from './item-search.component';
import { FeedDetailComponent } from './feed-detail.component';
import { FeedService } from './feed.service';

import { AppRoutingModule } from './app-routing.module';

import './rxjs-extensions';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    ItemDetailComponent,
    NewsComponent,
    DashboardComponent,
    ItemSearchComponent,
    FeedDetailComponent
  ],
  providers: [
    ItemService,
    FeedService
  ],
  bootstrap: [
    AppComponent
  ]
})

export class AppModule { }
