import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Item } from './item';
import { ItemDetailComponent } from './item-detail.component';
import { ItemService } from './item.service';

@Component({
  moduleId: module.id,
  selector: 'my-news',
  templateUrl: 'news.component.html',
  styleUrls: ['news.component.css']
})

export class NewsComponent implements OnInit {
  news: Item[];
  selectedItem: Item;

  constructor(
    private router: Router,
    private itemService: ItemService
  ) { }

  getNews(): void {
    this.itemService.getNews().then(news => this.news = news);
  }

  ngOnInit(): void {
    this.getNews();
  }

  onSelect(item: Item): void {
    this.selectedItem = item;
  }

  gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedItem.id]);
  }

  add(title: string): void {
    title = title.trim();
    if (!title) { return; }
    this.itemService.create(title)
        .then(item => {
	  this.news.push(item);
	  this.selectedItem = null;
	});
  }

  delete(item: Item): void {
    this.itemService
        .delete(item.id)
	.then(() => {
	  this.news = this.news.filter(i => i !== item);
	  if (this.selectedItem === item) { this.selectedItem = null; }
	});
  }
}
