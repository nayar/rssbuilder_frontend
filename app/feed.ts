export class Feed {
  id: number;
  title: string;
  description: string;
}