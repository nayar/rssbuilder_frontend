import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Item } from './item';
import { ItemService } from './item.service';

@Component({
  moduleId: module.id,
  selector: 'my-dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  news: Item[] = [];

  constructor(
  private router: Router,
  private itemService: ItemService) { }

  ngOnInit(): void {
    this.itemService.getNews()
      .then(news => this.news = news.slice(1,5));
  }

  gotoDetail(item: Item): void {
    let link = ['/detail', item.id];
    this.router.navigate(link);
  }
}