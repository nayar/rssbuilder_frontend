import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Feed } from './feed';

@Injectable()
export class FeedService {
  private feedsUrl = '/service/feeds';
  private headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Basic ' + btoa('test:test')});

  constructor(private http: Http) { }
  
  getFeed(): Promise<Feed> {
    return this.http.get(this.feedsUrl, {headers: this.headers})
               .toPromise()
               .then(response => {
	          var feed = new Feed;
	          var data = response.json();
		  if (data.length > 0) {
		    feed.id = 1;
		    feed.title = data[0].Title;
		    feed.description = data[0].Description;
		  } else {
		    feed.id = 0;
		    feed.title = '';
		    feed.description = '';
		  }
		  return feed;
	       })
	       .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occured', error);
    return Promise.reject(error.message || error);
  }

  update(feed: Feed): Promise<Feed> {
    const url = `${this.feedsUrl}/${feed.id}`;
    return this.http
               .put(url, JSON.stringify(feed), {headers: this.headers})
	       .toPromise()
	       .then(() => feed)
	       .catch(this.handleError);
  }

  create(title: string, description: string): Promise<Feed> {
    return this.http
               .post(this.feedsUrl, JSON.stringify({title: title, description: description}), {headers: this.headers})
	       .toPromise()
	       .then(res => {
	          var data = res.json();
		  var feed = new Feed();
		  feed.id = 1;
		  feed.title = data.Title;
		  feed.description = data.Description;
		  return feed;
	       })
	       .catch(this.handleError);
  }
}