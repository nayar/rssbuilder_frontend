import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Feed } from './feed';
import { FeedService } from './feed.service';

@Component({
  moduleId: module.id,
  selector: 'my-feed-detail',
  templateUrl: 'feed-detail.component.html'
})

export class FeedDetailComponent implements OnInit {
  feed: Feed;

  constructor(
    private feedService: FeedService,
    private router: Router
  ) {}

  getFeed(): void {
    this.feedService.getFeed().then(feed => this.feed = feed);
  }

  ngOnInit(): void {
    this.getFeed();
  }

  save(): void {
    if (this.feed.id == 0) {
      this.feedService.create(this.feed.title, this.feed.description)
                      .then(feed => this.feed = feed);
    }
    else {
      this.feedService.update(this.feed)
                      .then(feed => this.feed = feed);
    }
  }
}