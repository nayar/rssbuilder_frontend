import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs';

import { Item } from './item';

@Injectable()
export class ItemSearchService {
  private headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Basic ' + btoa('test:test')});

  constructor(private http: Http) {}

  search(term: string): Observable<Item[]> {
    return this.http
               .get(`/service/feeds/1/items/?name=${term}`, {headers: this.headers})
               .map((r: Response) => r.json().data as Item[]);
  }
}