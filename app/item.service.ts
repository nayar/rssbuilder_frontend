import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Item } from './item';

@Injectable()
export class ItemService {
  private newsUrl = '/service/feeds/1/items';
  private headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Basic ' + btoa('test:test')});

  constructor(private http: Http) { }
  
  getNews(): Promise<Item[]> {
    return this.http.get(this.newsUrl, {headers: this.headers})
               .toPromise()
               .then(response => {
	          var items = [];
	          var data = response.json();
		  for (var i = 0; i < data.length; i++) {
		    var item = new Item;
		    item.id = parseInt(data[i].Guid.Id);
		    item.title = data[i].Title;
		    items.push(item);
		  }
		  return items;
	       })
	       .catch(this.handleError);
  }

  getItem(id: number): Promise<Item> {
    return this.getNews()
      .then(news => news.find(item => item.id === id));
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occured', error);
    return Promise.reject(error.message || error);
  }

  update(item: Item): Promise<Item> {
    const url = `${this.newsUrl}/${item.id}`;
    return this.http
               .put(url, JSON.stringify({title: item.title, description: item.title}), {headers: this.headers})
	       .toPromise()
	       .then(() => item)
	       .catch(this.handleError);
  }

  create(title: string): Promise<Item> {
    return this.http
               .post(this.newsUrl, JSON.stringify({title: title, description: title}), {headers: this.headers})
	       .toPromise()
	       .then(res => {
	          var data = res.json();
		  var item = new Item();
		  item.id = parseInt(data.Guid.Id);
		  item.title = data.Title;
		  return item;
	       })
	       .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.newsUrl}/${id}`;
    return this.http
               .delete(url, {headers: this.headers})
	       .toPromise()
	       .then(() => null)
	       .catch(this.handleError);
  }
}
